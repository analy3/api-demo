<?php
/*
*
* @project		:	API-demo
*
* @description	:	This API demo demonstrates how interactive websites can work without the use of templates (code and placeholders in HTML)
*
* @license		:	https://creativecommons.org/licenses/by-sa/4.0/
*
* @author		:	Mike P.J. van Dartel <dartello@gmail.com>
*
*
*
* @date			:	Tuesday, 21 January 2020
*
* @launch		:
*
*
*
* @class		:	Class( $member )
*
* @members		:	- = private; + = public; o = static
*
* @includes		:	
*
* @remarks		:
*
* @change log	:
*
*/
session_start();
$relative_dir = "../";
include "../include/getserver.php";

$index_page = true;
$year = date("Y", filemtime("logs.php"));

include "../include/queries.php";

if($_GET) {
	if(array_key_exists("nlogs", $_GET) && is_numeric($_GET["nlogs"])) {
		$nlogs = $_GET["nlogs"];
	}
}

class Logs
{
	private $connect;

	public function __construct(mysqli $connect)
	{
		$this->connect = $connect;
	}

	public function getLogs()
	{
		$query = "SELECT * FROM `logs` ORDER BY datum DESC";
		return query_array($this->connect, $query);
	}

	public function LastEntries(int $n)
	{
		$query = "SELECT *, (meetwaarde1+meetwaarde2) AS total FROM `logs` ORDER BY datum DESC LIMIT ".$n;
		return query_array($this->connect, $query);
	}
}

$logs = new Logs($cxn);
if(isset($nlogs)) {
	echo json_encode($logs->LastEntries($nlogs));
} else {
	echo json_encode($logs->getLogs());
}
?>
