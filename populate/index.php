<?php
/*
*
* @project		:	API-demo
*
* @description	:	This API demo demonstrates how interactive websites can work without the use of templates (code and placeholders in HTML)
*
* @license		:	https://creativecommons.org/licenses/by-sa/4.0/
*
* @author		:	Mike P.J. van Dartel <dartello@gmail.com>
*
*
*
* @date			:	Tuesday, 21 January 2020
*
* @kick-off		:	Tuesday, 21 Januari 2020
*
* @launch		:	
*
*
*
* @content		:	Populate database
*
* @remarks		:
*
* @change log	:
*
*/

session_start();
$relative_dir = "../";
include $relative_dir."include/getserver.php";

$index_page = true;
$year = date("Y", filemtime("index.php"));

include $relative_dir."include/queries.php";

mysqli_query($cxn, "INSERT INTO logs (meetwaarde1, meetwaarde2) VALUES (".rand(10, 99999).", ".rand(10, 99999).")");
?>