/*
*
* @project		:	API-demo
*
* @description	:	This API demo demonstrates how interactive websites can work without the use of templates (code and placeholders in HTML)
*
* @license		:	https://creativecommons.org/licenses/by-sa/4.0/
*
* @author		:	Mike P.J. van Dartel <dartello@gmail.com>
*
*
*
* @date			:	Tuesday, 21 January 2020
*
*
*
* @content		:
* 					1. Select Database
* 					2. Delete tables if present
* 					3. Create tables
* 					4. Fill tables
*					5. Select data (Deprecated)
*
* @remarks		:
*
* @change log	:
*
*/

-- 1. Selecteer databank

USE api_db;




-- 2. Verwijder tabellen mits aanwezig

DROP TABLE IF EXISTS logs;






-- 3. Maak tabellen aan

CREATE TABLE IF NOT EXISTS updates (
	id int(11) NOT NULL AUTO_INCREMENT,
	updated_at timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	key(updated_at),
	primary key(id)
);


CREATE TABLE logs (
	id int(11) NOT NULL AUTO_INCREMENT,
	datum timestamp NOT NULL,
	meetwaarde1 int(5) NOT NULL,
	meetwaarde2 int(5) NOT NULL,

	primary key(id)
);






-- 4. Vul de tabellen
SET sql_mode = '';

INSERT INTO updates () VALUES
();

