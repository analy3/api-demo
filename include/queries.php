<?php
/*
*
* @project		:	API-demo
*
* @description	:	This API demo demonstrates how interactive websites can work without the use of templates (code and placeholders in HTML)
*
* @license		:	https://creativecommons.org/licenses/by-sa/4.0/
*
* @author		:	Mike P.J. van Dartel <dartello@gmail.com>
*
*
*
* @date			:	Tuesday, 21 January 2020
*
* @launch		:	
*
*
*
* @content		:	Collection of database queries with the required PHP-functions, constants and objects
*
* @remarks		:
*
* @change log	:
*
*/


if(!isset($index_page)) {
	header("Location: ../");
	exit;
}


/* LIST OF CONSTANTS */
//define("", "");

/* LIST OF REGULAR EXPRESSIONS */
define("REGEX_ENUM", "/^enum\(\'(.*)\'\)$/");




/* FUNCTIONS */

function human_readable_version($versionstring)
{
	if(!is_string($versionstring)) return null;

	return substr($versionstring, 0, strpos($versionstring, ".", 2));
}


function query_array($cxn, $query, &$fieldnames = null)
{
	if(!is_object($cxn) || empty($cxn) || !is_string($query)) return null;

	$result = mysqli_query($cxn, $query);
	if(!$result) return null;

	$row = mysqli_fetch_assoc($result);

	if(!$row) return null;

	$fieldnames = array_keys($row);

	$array = array();

	do {
		$array[] = $row;
	} while($row = mysqli_fetch_assoc($result));

	return $array;
}


function enlist_options($array, $encode = true)
{
	if(!count($array)) return null;

	$values = array();

	foreach($array as $value) {
		if(!empty($value)) {
			$keys = array_keys($value);
			if($encode) {
				$values[$value[$keys[0]]] = htmlspecialchars($value[$keys[1]], ENT_COMPAT, "ISO-8859-1");
			} else {
				$values[$value[$keys[0]]] = $value[$keys[1]];
			}
		}
	}

	return $values;
}


function get_enum_values($cxn, $table, $field)
{
	$type = query_array($cxn, "SHOW COLUMNS FROM ".$table." WHERE Field = '".$field."'" )[0]["Type"];

	if(preg_match(REGEX_ENUM, $type, $matches)) {
		return explode("','", $matches[1]);
	}

	return null;
}


function sumsUp($array)
{
	if(empty($array) || !count($array)) return null;

	return (count($array) > 1 ? implode(", ", explode("^", implode("^", $array), -1))." and " : "").end($array);
}


function mb_ucfirst($str, $lowercase = false) {
	$fc = $lowercase ? mb_strtolower(mb_substr($str, 0, 1)) : mb_strtoupper(mb_substr($str, 0, 1));

	return $fc.mb_substr($str, 1);
}

function mysqlToJavaScriptKeys($mysql_array)
{
	if(empty($mysql_array) || !count($mysql_array)) return $mysql_array;

	$javaScriptArray = array();
	foreach($mysql_array as $key => $value) {
		if(!is_numeric($key)) {
			$underscoresRemoved = explode("_", $key);
			$itemsCapitalized = array_map("mb_ucfirst", $underscoresRemoved);
			$javaScriptArray[mb_ucfirst(implode("", $itemsCapitalized), true)] = $value;
		} else {
			$javaScriptArray[$key] = $value;
		}
	}

	return $javaScriptArray;
}






function recVisit($cxn, $visit, $error, $member)
{
	/* Validate Parameters */
	if(!is_object($cxn) || !is_array($visit) || empty($cxn) || empty($visit)) return null;

	/* Validate Input Array */
	if(!array_key_exists("year", $visit) || !array_key_exists("Host", $visit) || !array_key_exists("Client", $visit)) return false;

	/* Compose Query */
	$loc = array();
	if(array_key_exists("loc", $visit["Host"]) && !empty($visit["Host"]["loc"])) {
		$loc = explode(",", $visit["Host"]["loc"]);
	}

	$values = array(
		"stamp" => $visit["year"],
		"host_ip" => array_key_exists("ip", $visit["Host"]) && !empty($visit["Host"]["ip"]) ? $visit["Host"]["ip"] : "NULL",
		"host_hostname" => array_key_exists("hostname", $visit["Host"]) && !empty($visit["Host"]["hostname"]) ? $visit["Host"]["hostname"] : "NULL",
		"host_city" => array_key_exists("city", $visit["Host"]) && !empty($visit["Host"]["city"]) ? $visit["Host"]["city"] : "NULL",
		"host_region" => array_key_exists("region", $visit["Host"]) && !empty($visit["Host"]["region"]) ? $visit["Host"]["region"] : "NULL",
		"host_country" => array_key_exists("country", $visit["Host"]) && !empty($visit["Host"]["country"]) ? $visit["Host"]["country"] : "NULL",
		"host_location_NW" => count($loc) ? $loc[0] : "NULL",
		"host_location_ZO" => count($loc) ? $loc[1] : "NULL",
		"host_company" => array_key_exists("org", $visit["Host"]) && !empty($visit["Host"]["org"]) ? $visit["Host"]["org"] : "NULL",
		"host_postal_code" => array_key_exists("postal", $visit["Host"]) && !empty($visit["Host"]["postal"]) ? $visit["Host"]["postal"] : "NULL",
		"client_ip" => array_key_exists("ip", $visit["Client"]) && !empty($visit["Client"]["ip"]) ? $visit["Client"]["ip"] : "NULL",
		"client_visited" => array_key_exists("visited", $visit["Client"]) && !empty($visit["Client"]["visited"]) ? $visit["Client"]["visited"] : "NULL",
		"client_useragent" => array_key_exists("user agent", $visit["Client"]) && !empty($visit["Client"]["user agent"]) ? $visit["Client"]["user agent"] : "NULL",
	);

	$gooddata = array();
	foreach($values as $key => $values) {
		$gooddata[$key] = str_replace("'", "&rsquo;", htmlspecialchars(trim(strip_tags($values))));
	}

	$_SESSION["bezoeker"]["stamp"] = $gooddata["stamp"];
	$_SESSION["bezoeker"]["host"]["ip"] = $gooddata["host_ip"];
	$_SESSION["bezoeker"]["host"]["hostname"] = $gooddata["host_hostname"];
	$_SESSION["bezoeker"]["host"]["city"] = $gooddata["host_city"];
	$_SESSION["bezoeker"]["host"]["region"] = $gooddata["host_region"];
	$_SESSION["bezoeker"]["host"]["country"] = $gooddata["host_country"];
	$_SESSION["bezoeker"]["host"]["location"]["NW"] = $gooddata["host_location_NW"];
	$_SESSION["bezoeker"]["host"]["location"]["ZO"] = $gooddata["host_location_ZO"];
	$_SESSION["bezoeker"]["host"]["company"] = $gooddata["host_company"];
	$_SESSION["bezoeker"]["host"]["postal code"] = $gooddata["host_postal_code"];
	$_SESSION["bezoeker"]["client"]["ip"] = $gooddata["client_ip"];
	$_SESSION["bezoeker"]["client"]["visited"] = $gooddata["client_visited"];
	$_SESSION["bezoeker"]["client"]["user agent"] = $gooddata["client_useragent"];
	$_SESSION["bezoeker"]["client"]["error"] = $error;
	$_SESSION["bezoeker"]["client"]["memeber"] = $member;

	unset($gooddata["stamp"]);
	$keys = "(".implode(", ", array_keys($gooddata)).", client_error, client_member)";
	$values = "('".implode("', '", array_values($gooddata))."', '".$error."', '".$member."')";

	$query = "INSERT INTO bezoeken ".$keys." VALUES ".$values;

	/* Execute Query */
	if(mysqli_query($cxn, $query)) {
		return query_array($cxn, "SELECT id FROM bezoeken ORDER BY stamp DESC")[0]["id"];
	}

	return false;
}






/* DATABASE CONNECTION */

/*
	* phpmyadmin
	http://remote.phpmyadmin-host.com/
*/
if(preg_match("/(www.)?domain.nl/", $_SERVER["SERVER_NAME"])) {
	$hostname = "";
	$username = "";
	$password = "";
	$database = "";
} elseif($_SESSION["local"]) {
	$hostname = "localhost";
	$username = "root";
	$password = "master";
	$database = "api_db";
}

$DBerror = "No database connection: A database is required for this website.<br />Het systeem heeft geen databankverbinding kunnen maken: Een databank is nodig om de website te bedienen.<br />De website is gestopt met laden.";
if(empty($database) || $database !== "api_db") {
	if($_SESSION["local"]) {
		var_dump($_SERVER["SERVER_NAME"], $database);
	}
	die($DBerror);
}

$cxn = mysqli_connect($hostname, $username, $password, $database) or die ($DBerror);

$sharepicture = "img/sharepicture.png";
$phpversion   = human_readable_version(phpversion());

if(!empty($cxn)) {
	$mysqlversion = human_readable_version(mysqli_get_server_info($cxn));
}
/* QUERIES */
/*
$xQuery =
"SELECT
	*
FROM table
JOIN other ON other.id = table.other
WHERE t > 1
ORDER BY name";
$x = query_array($cxn, $xQuery);
*/

