<?php
/*
*
* @project		:	API-demo
*
* @description	:	This API demo demonstrates how interactive websites can work without the use of templates (code and placeholders in HTML)
*
* @license		:	https://creativecommons.org/licenses/by-sa/4.0/
*
* @author		:	Mike P.J. van Dartel <dartello@gmail.com>
*
*
*
* @date			:	Tuesday, 21 January 2020
*
* @kick-off		:	Tuesday, 21 Januari 2020
*
* @launch		:	
*
*
*
* @content		:	Defines the server name
*
* @remarks		:
*
* @change log	:
*
*/

define("REGEX_IP_ADDR", "/^(2([0-4]\d|5[0-5])|1?\d{1,2})\.(2([0-4]\d|5[0-5])|1?\d{1,2})\.(2([0-4]\d|5[0-5])|1?\d{1,2})\.(2([0-4]\d|5[0-5])|1?\d{1,2})$/");

if(!isset($relative_dir) || !file_exists($relative_dir)) {
	$relative_dir = "";
}

$file = $relative_dir."local-server";

$_SESSION["server"] = "";

if(file_exists($file)) {
	$_SESSION["server"] = trim(file_get_contents($file));

	if($_SESSION["local"] = in_array($_SERVER["SERVER_NAME"], array("localhost", "127.0.0.1", $_SESSION["server"]))) {
		/* ini_set */
		ini_set('error_reporting', E_ALL);
		ini_set('display_errors', TRUE);
		ini_set('output_buffering', 4096);
	}

	if(!preg_match(REGEX_IP_ADDR, $_SESSION["server"])) {
		echo "Unexpected contents in ".$file;
		exit;
	}
} elseif(preg_match(REGEX_IP_ADDR, $_SERVER["SERVER_NAME"]) && $_SERVER["SERVER_NAME"] !== "127.0.0.1") {
		echo "No configuration file found for ".$_SERVER["SERVER_NAME"];
		exit;
}
?>
