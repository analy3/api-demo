/*
*
* @project		:	API-demo
*
* @description	:	This API demo demonstrates how interactive websites can work without the use of templates (code and placeholders in HTML)
*
* @license		:	https://creativecommons.org/licenses/by-sa/4.0/
*
* @author		:	Mike P.J. van Dartel <dartello@gmail.com>
*
*
*
* @date			:	Tuesday, 21 January 2020
*
* @launch		:	
*
*
*
* @content		:	JS file
*
* @remarks		:
*
* @change log	:
*
*/

Number.prototype.leadingZero = function(n = 1)
{
	if(!(this instanceof Number)) return null;

	var number = parseInt(this);

	if(isNaN(number)) return NaN;

	var leadingZero = "0";

	return leadingZero.repeat(n) + number;
}

Date.prototype.getFullMonth = function(fullLength = 1)
{
	if(!(this instanceof Date)) return null;

	var month;

	if(fullLength) {
		month = this.getMonth();

		switch(month) {
			case 0: month = "January"; break;
			case 1: month = "February"; break;
			case 2: month = "March"; break;
			case 3: month = "April"; break;
			case 4: month = "May"; break;
			case 5: month = "June"; break;
			case 6: month = "July"; break;
			case 7: month = "August"; break;
			case 8: month = "September"; break;
			case 9: month = "October"; break;
			case 10: month = "November"; break;
			case 11: month = "December"; break;
		}
	} else {
		month = this.toString().split(" ");
		month = month[1];
	}

	return month;
}

String.prototype.commonDate = function()
{
	var d = this instanceof Object ? new Date(this.toString()) : new Date(this);

	if(isNaN(d)) {
		if(this.toString().match(/^00?(00)?\-00?\-00?$/)) {
			return "Unknown";
		} else if(this.toString().match(/^\d{4}\-00?\-00?$/)) {
			return this.toString().match(/^\d{4}/);
		}
	}

	return (
		d.getFullMonth() + " " +
		d.getDate() + ", " +
		d.getFullYear()
	);
}

String.prototype.humanDate = function()
{
	var d = this instanceof Object ? new Date(this.toString()) : new Date(this);

	if(isNaN(d)) {
		if(this.toString().match(/^00?(00)?\-00?\-00?$/)) {
			return "Unknown";
		} else if(this.toString().match(/^\d{4}\-00?\-00?$/)) {
			return this.toString().match(/^\d{4}/);
		}
	}

	var today = new Date();

	if(today.getMonth() == d.getMonth() && today.getFullYear() == d.getFullYear()) {
		switch(d.getDate() - today.getDate()) {
			case -1: return "Yesterday, " + d.toLocaleTimeString();
			case 0: return "Today, " + d.toLocaleTimeString();
			case 1: return "Tomorrow, " + d.toLocaleTimeString();
			default: return (
				d.getFullMonth() + " " +
				d.getDate() + ", " +
				d.getFullYear() + ", " +
				d.toLocaleTimeString()
			);
		}
	}

	return (
		d.getFullMonth() + " " +
		d.getDate() + ", " +
		d.getFullYear() + ", " +
		d.toLocaleTimeString()
	);
}

String.prototype.htmlspecialcharsDecode = function()
{
	if(!(this instanceof String)) return null;

	var parser = new DOMParser;
	var dom = parser.parseFromString("<!doctype html><body>" + this, "text/html");

	return dom.body.textContent;
}


var nlogs = 4;
var valueLength = 4;
var maxValue = 99999;
var barHeight = 350;
var animationSpeed = 1000;

var contentWidth = $("#bar-graphic-perc > div").width();

$(document).ready(function() {
	$(window).resize(function() {
		contentWidth = $("#bar-graphic-perc > div").width();
	});

//	setInterval(function() {
		$.getJSON("json/logs.php?nlogs=" + nlogs, function(logs) {
		}).fail(function(jqXHR, textStatus, errorThrown) {
			alert("Logs: " + jqXHR.responseText);
		}).done(function(logs) {
			var totals = [];
			$.each(logs, function(index, log) {
				totals.push(log.total);
			});
			var max = Math.max(...totals);
//			console.log(Math.max(...totals));

			while($("#bar-graphic > div.left").length > nlogs) {
				$("#bar-graphic > div.left:last-of-type")
					.remove();
			}

			while($("#bar-graphic-perc > div > div.left").length > nlogs) {
				$("#bar-graphic-perc > div > div:first-of-type")
					.remove();
			}

			while($("#logs-table > div.left").length > nlogs) {
				$("#logs-table > div:last-of-type")
					.remove();
			}

			$.each(logs, function(index, log) {
				var values = [Math.min(log.meetwaarde1, log.meetwaarde2), Math.max(log.meetwaarde1, log.meetwaarde2)];
				var logdate = new Date(log.datum);

				$("#bar-graphic")
					.prepend("<div></div>")
					.children(":first-child")
					.addClass("left")
					.append("<div></div>")
					.children()
					.append("<div></div>")
					.children()
					.append("<div></div>")
					.append("<div></div>")
					.children(":first-child")
					.addClass("bars")
					.append("<div></div>")
					.children()
					.append("<div></div>")
					.append("<div></div>")
					.append("<div></div>")
					.children(":not(:last-child)")
					.addClass("left")
					.append("<div></div>")
					.append("<div></div>")
					.parent()
					.children(":first-child")
					.css({
						top: 0 - (barHeight/maxValue * log.meetwaarde1) + "px"
					})
					.children(":first-child")
					.css({
						top: barHeight/maxValue * log.meetwaarde1 + "px"
					})
					.animate({
						top: 0
					}, animationSpeed)
					.text(0)//log.meetwaarde1)
					.next()
					.css({
						top: barHeight/maxValue * log.meetwaarde1 + "px"
					})
					.animate({
						height: barHeight/maxValue * log.meetwaarde1 + "px",
						top: 0
					}, animationSpeed)
					.addClass("value1")
					.parent()
					.next()
					.css({
						top: 0 - (barHeight/maxValue * log.meetwaarde2) + "px"
					})
					.children(":first-child")
					.css({
						top: barHeight/maxValue * log.meetwaarde2 + "px"
					})
					.animate({
						top: 0
					}, animationSpeed)
					.text(0)//log.meetwaarde2)
					.next()
					.css({
						top: barHeight/maxValue * log.meetwaarde2 + "px"
					})
					.animate({
						height: barHeight/maxValue * log.meetwaarde2 + "px",
						top: 0
					}, animationSpeed)
					.addClass("value2")
					.parent()
					.parent()
					.children(":last-child")
					.addClass("clear")
					.parent()
					.parent()
					.parent()
					.children(":last-child")
					.append("<div></div>")
					.append("<div></div>")
					.append("<div></div>")
					.children(":first-child")
					.text("ID: " + log.id)
					.next()
					.text(logdate.toLocaleDateString())
					.next()
					.text(logdate.toLocaleTimeString());

				$("#bar-graphic-perc > div")
					.prepend("<div></div>")
					.children(":first-child")
					.addClass("gr-perc")
					.append("<div></div>")
					.append("<div></div>")
					.children(":first-child")
					.append("<span></span>")
					.append("<span></span>")
					.children(":first-child")
					.text("ID: " + log.id)
					.next()
					.text(logdate.toLocaleDateString() + " " + logdate.toLocaleTimeString())
					.parent()
					.parent()
					.children(":last-child")
					.append("<div></div>")
					.append("<div></div>")
					.append("<div></div>")
					.children(":not(:last-child)")
					.addClass("left")
					.parent()
					.children(":first-child")
					.addClass("value1")
					.width(100/log.total * log.meetwaarde1 + "%")
					.append("<div></div>")
					.children()
					.text((100/log.total * log.meetwaarde1).toPrecision(valueLength) + "%")
					.parent()
					.parent()
					.children(":nth-child(2)")
					.addClass("value2")
					.width(100/log.total * log.meetwaarde2 + "%")
					.append("<div></div>")
					.children()
					.text((100/log.total * log.meetwaarde2).toPrecision(valueLength) + "%")
					.parent()
					.parent()
					.children(":last-child")
					.addClass("clear");

				$("#logs-table > div > div:last-child")
					.append("<div></div>")
					.children(":last-child")
					.append("<span></span>")
					.append("<span></span>")
					.append("<span></span>")
					.append("<span></span>")
					.children(":first-child")
					.text("ID: " + log.id)
					.next()
					.text(log.datum.humanDate())
					.next()
					.text(log.meetwaarde1)
					.next()
					.text(log.meetwaarde2);
			});

			$("#bar-graphic, #bar-graphic-perc > div")
				.append("<div></div>")
				.children(":last-child")
				.addClass("clear");

			setInterval(function() {
				$("#bar-graphic div.bars > div > div.left").each(function() {
					var barHeightText = Math.trunc($(this).children(":last-child").height() * maxValue / barHeight);
					$(this)
						.children(":first-child")
						.text(barHeightText);
				});
			}, 1);
		});
//	}, 1000);
});