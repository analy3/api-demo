<?php
/*
*
* @project		:	API-demo
*
* @description	:	This API demo demonstrates how interactive websites can work without the use of templates (code and placeholders in HTML)
*
* @license		:	https://creativecommons.org/licenses/by-sa/4.0/
*
* @author		:	Mike P.J. van Dartel <dartello@gmail.com>
*
*
*
* @date			:	Tuesday, 21 January 2020
*
* @kick-off		:	Tuesday, 21 Januari 2020
*
* @launch		:	
*
*
*
* @content		:	Index page
*
* @remarks		:
*
* @change log	:
*
*/

session_start();
include "include/getserver.php";

$index_page = true;
$year = date("Y", filemtime("index.php"));

include "include/queries.php";

?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- meta standard -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- meta author -->
<meta name="author" content="analy3.org - M P J van Dartel" />
<!-- meta description of this page -->
<meta name="description" content="This API demo demonstrates how interactive websites can work without the use of templates (code and placeholders in HTML)" />
<!-- meta web searches -->
<meta name="keywords" content="API, PHP, JavaScript, JS, Jquery, MySQL" />

<?php
if(!empty($sharepicture) && file_exists($sharepicture)) {
	list($width, $height, $type, $attr) = getimagesize($sharepicture);
?>
<meta property="og:image" <?php echo "content=\"".$sharepicture."\""; ?> />
<meta property="og:image:type" <?php echo "content=\"".mime_content_type($sharepicture)."\""; ?> />
<meta property="og:image:width" <?php echo "content=\"".$width."\""; ?> />
<meta property="og:image:height" <?php echo "content=\"".$height."\""; ?> />
<?php
}
?>

<!-- title of the page -->
<title>API-demo</title>

<!-- icon -->
<link rel="icon" type="image/x-icon" href="img/favicon.gif">
<link rel="bookmark icon" type="image/x-icon" href="img/favicon.gif">
<link rel="SHORTCUT ICON" type="image/x-icon" href="img/favicon.gif">

<!-- language -->
<link rel="alternate" hreflang="en" href="http://en.example.com/" />

<!-- style sheets -->
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" /> <!-- http://fontawesome.io/icons/ -->
<link rel="stylesheet" type="text/css" href="css/general.css" />

<!-- client based script -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script type="text/javascript" src="js/general.js"></script>
</head>

<body>
	<div class="center">
		<div>
			<div class="gs960">
				<div id="bar-graphic">
				</div>
				<div id="bar-graphic-perc">
					<div>
					</div>
				</div>
				<div id="logs-table">
					<div>
						<div>
							<div>
								<span>ID</span><span>Date</span><span>Value 1</span><span>Value 2</span>
							</div>
						</div>
						<div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>
